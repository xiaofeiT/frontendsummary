# 前端知识体系总结

本文面向刚参加工作的前端开发者，因此更多的是总结分析基础类知识，同时包含部分进阶技能。

当然，随着内容的更新增加，可能基础类知识比例会减少。

## Reading 

1. [xiaofeit.gitlab.io/frontendsummary/](https://xiaofeit.gitlab.io/frontendsummary/) 
  
    使用的 Gitlab 自动构建，更新即使，访问加载速度稍慢

2. [front.newfiworld.xyz](http://front.newfiworld.xyz) 

    自建站点，更新可能会滞后，访问加载速度稍微快点


## Other

任何形式的建议都是欢迎的。
